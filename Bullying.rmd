---
title: "BHRC Bullying"
author: "André Simioni"
date: "5 de setembro de 2019"
output: html_document
---
# Librarys and settings
First, read the README.md section about `settings.r` file.
```{r lib,  include=FALSE}
# Aditional librarys

# Load settings.r
source("settings.r", echo = T)
```

# Get data from Redcap
```{r}
hrc_raw <- import(bhrc_noidraw, setclass = "tibble") %>% 
  filter(redcap_event_name != "screening_arm_1")
```

# Compute
```{r}
bullying <- hrc_raw %>% 
  select(ident, redcap_event_name, 
         p_bulliedhim_l = fat22, # parental report suffered bullying in life W0
         p_bulliedoth_l = fat23, # parental report bullied others in life W0
         c_bulliedhim_l = p_fat22, # child report suffered bullying in life W0
         c_bulliedoth_l = p_fat23, # child report suffered bullying in life W0
         matches("fat22"), matches("fat23")
  ) %>% 
  unite(p_bulliedhim, fat22a, nfat22, sep = "") %>% # parental report suffered bullying this year
  unite(p_bulliedhim_fr, fat22b, nfat22a, sep = "") %>%
  unite(p_bulliedhim_imp, fat22c, nfat22b, sep = "") %>%
  unite(p_bulliedoth, fat23a, nfat23, sep = "") %>% # parental report practiced bullying this ye
  unite(p_bulliedoth_fr, fat23b, nfat23a, sep = "") %>%
  unite(p_bulliedoth_imp, fat23c, nfat23b, sep = "") %>%
  unite(c_bulliedhim, p_fat22a, p_nfat22, sep = "") %>% # child report suffered bullying this year
  unite(c_bulliedhim_fr, p_fat22b, p_nfat22a, sep = "") %>%
  unite(c_bulliedhim_imp, p_fat22c, p_nfat22b, sep = "") %>%
  unite(c_bulliedoth, p_fat23a, p_nfat23, sep = "") %>%  # child report practiced bullying this year
  unite(c_bulliedoth_fr, p_fat23b, p_nfat23a, sep = "") %>%
  unite(c_bulliedoth_imp, p_fat23c, p_nfat23b, sep = "") %>% 
  mutate(across(matches("bullied"), ~str_remove_all(., "NA") %>% na_if("") %>% as.numeric),
         p_bulliedhim = case_when(p_bulliedhim_l == 0 ~ 0, 
                                  p_bulliedhim_l == 99 ~ 99, 
                                  TRUE ~ as.numeric(p_bulliedhim)),
         p_bulliedhim_fr = case_when(p_bulliedhim_l == 0 ~ 0, 
                                     p_bulliedhim_l == 99 ~ 99, 
                                     p_bulliedhim == 0 ~ 0, 
                                     p_bulliedhim == 99 ~ 99, 
                                     TRUE ~ as.numeric(p_bulliedhim_fr)),
         p_bulliedhim_imp = case_when(p_bulliedhim_l == 0 ~ 0, 
                                      p_bulliedhim_l == 99 ~ 99, 
                                      p_bulliedhim == 0 ~ 0, 
                                      p_bulliedhim == 99 ~ 99, 
                                      TRUE ~ as.numeric(p_bulliedhim_imp)),
         p_bulliedoth = case_when(p_bulliedoth_l == 0 ~ 0, 
                                  p_bulliedoth_l == 99 ~ 99, 
                                  TRUE ~ as.numeric(p_bulliedoth)),
         p_bulliedoth_fr = case_when(p_bulliedoth_l == 0 ~ 0, 
                                     p_bulliedoth_l == 99 ~ 99, 
                                     p_bulliedoth == 0 ~ 0, 
                                     p_bulliedoth == 99 ~ 99, 
                                     TRUE ~ as.numeric(p_bulliedoth_fr)),
         p_bulliedoth_imp = case_when(p_bulliedoth_l == 0 ~ 0, 
                                      p_bulliedoth_l == 99 ~ 99, 
                                      p_bulliedoth == 0 ~ 0, 
                                      p_bulliedoth == 99 ~ 99, 
                                      TRUE ~ as.numeric(p_bulliedoth_imp)),
         c_bulliedhim = case_when(c_bulliedhim_l == 0 ~ 0, # child report
                                  c_bulliedhim_l == 99 ~ 99, 
                                  TRUE ~ as.numeric(c_bulliedhim)),
         c_bulliedhim_fr = case_when(c_bulliedhim_l == 0 ~ 0, 
                                     c_bulliedhim_l == 99 ~ 99, 
                                     c_bulliedhim == 0 ~ 0, 
                                     c_bulliedhim == 99 ~ 99, 
                                     c_bulliedhim_fr == 88 ~ 0,
                                     TRUE ~ as.numeric(c_bulliedhim_fr)),
         c_bulliedhim_imp = case_when(c_bulliedhim_l == 0 ~ 0, 
                                      c_bulliedhim_l == 99 ~ 99, 
                                      c_bulliedhim == 0 ~ 0, 
                                      c_bulliedhim == 99 ~ 99, 
                                      c_bulliedhim_imp == 88 ~ 0,
                                      TRUE ~ as.numeric(c_bulliedhim_imp)),
         c_bulliedoth = case_when(c_bulliedoth_l == 0 ~ 0, # child report
                                  c_bulliedoth_l == 99 ~ 99, 
                                  TRUE ~ as.numeric(c_bulliedoth)),
         c_bulliedoth_fr = case_when(c_bulliedoth_l == 0 ~ 0, 
                                     c_bulliedoth_l == 99 ~ 99, 
                                     c_bulliedoth == 0 ~ 0, 
                                     c_bulliedoth == 99 ~ 99, 
                                     c_bulliedoth_fr == 88 ~ 0,
                                     TRUE ~ as.numeric(c_bulliedoth_fr)),
         c_bulliedoth_imp = case_when(c_bulliedoth_l == 0 ~ 0, 
                                      c_bulliedoth_l == 99 ~ 99, 
                                      c_bulliedoth == 0 ~ 0, 
                                      c_bulliedoth == 99 ~ 99, 
                                      c_bulliedoth_imp == 88 ~ 0,
                                      TRUE ~ as.numeric(c_bulliedoth_imp))) %>% 
  set_na(p_bulliedhim_l:p_bulliedoth_imp, c_bulliedhim_l:c_bulliedoth_imp, na = c(99,77)) %>% 
  # set var/value labels
  set_labels(p_bulliedhim_l, p_bulliedoth_l, c_bulliedhim_l, c_bulliedoth_l, p_bulliedhim, p_bulliedoth, c_bulliedhim, c_bulliedoth, labels = c(Yes = 1, No = 0)) %>% 
  set_labels(ends_with("_fr"), labels = c(Never=0, "Once or twice this year"=1, "Sometimes, once in a while"=2, "About once a week"=3, "Several times a week / almost every day"=4, "Every day"=5)) %>%
  set_labels(ends_with("_imp"), labels = c(Nothing=0, "A little"=1, Much=2, Extremely=3)) %>% 
  var_labels(p_bulliedhim_l = "Has the child ever been bullied in his life?",
             p_bulliedoth_l = "Has the child ever done bullying in his life?",
             c_bulliedhim_l = "Have you ever been bullied in your life?",
             c_bulliedoth_l = "Have you ever done bullying?",
             p_bulliedhim = "Has the child ever been bullied this year?", # parental report
             p_bulliedhim_fr = "If yes, how often has the child been bullied this year?",
             p_bulliedhim_imp = "How much did the bullying bother the child?",
             p_bulliedoth = "Did the child ever bullied other child in this year?",
             p_bulliedoth_fr = "How often the child did bullying in the last year?",
             p_bulliedoth_imp = "Has the child ever felt guilty for practicing bullying?",
             c_bulliedhim = "Have you been bullied in the last 12 months?", # child report
             c_bulliedhim_fr = "If yes, how often do you suffered bullying in the last 12 months?",
             c_bulliedhim_imp = "How much did it bothered you?",
             c_bulliedoth = "Have you done bullying against other child in the last 12 months?",
             c_bulliedoth_fr = "If yes, how often have you done bullying in the last 12 months?",
             c_bulliedoth_imp = "Did you feel guilty about having that kind of behavior?")
# summary
bullying
bullying %>% names()
bullying %>% wave(2) %>% sumfac(.label = F)
```

# Export
```{r}
bullying %>% export("bhrc_data/Proc/Bullying.rds")
```


#!/usr/bin/env bash

# Bash script to rearrange E-PRIME BHRC files
# Run the following command in bash to make the script executable:
# chmod u+x arrange_eprime_w2.bash

## first, set variables with path names before run the script
ORIGIN_RS=~/Documentos/HRC/Eprime/W2/Unordered/RS/
ORIGIN_SP=~/Documentos/HRC/Eprime/W2/Unordered/SP/
DESTINY=~/Documentos/HRC/Eprime/W2/

## create subfolders
mkdir -p $DESTINY/Ordered/{RS,SP}/{A-GNG,C-MSTR,G-2CR,H-S400,K-DOTS}

## define DESTINY
DESTINY_RS=$DESTINY/Ordered/RS
DESTINY_SP=$DESTINY/Ordered/SP

## find files and copy to DESTINY
find $ORIGIN_RS -name "*A-GNG*" -exec cp '{}' $DESTINY_RS/A-GNG \;
find $ORIGIN_RS -name "*C-MSTR*" -exec cp '{}' $DESTINY_RS/C-MSTR \;
find $ORIGIN_RS -name "*G-2CR*" -exec cp '{}' $DESTINY_RS/G-2CR \;
find $ORIGIN_RS -name "*H-S400*" -exec cp '{}' $DESTINY_RS/H-S400 \;
find $ORIGIN_RS -name "*K-DOT*" -exec cp '{}' $DESTINY_RS/K-DOTS \;

find $ORIGIN_SP -name "*A-GNG*" -exec cp '{}' $DESTINY_SP/A-GNG \;
find $ORIGIN_SP -name "*C-MSTR*" -exec cp '{}' $DESTINY_SP/C-MSTR \;
find $ORIGIN_SP -name "*G-2CR*" -exec cp '{}' $DESTINY_SP/G-2CR \;
find $ORIGIN_SP -name "*H-S400*" -exec cp '{}' $DESTINY_SP/H-S400 \;
find $ORIGIN_SP -name "*K-DOT*" -exec cp '{}' $DESTINY_SP/K-DOTS \;

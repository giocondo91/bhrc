# Open standard libraries
library(rio); library(rlang); library(tidyverse); library(sjmisc); library(sjlabelled);

# Setting working directory
setwd("~/Documentos/pCloud/Projetos/Analises/bhrc") # Andre
bhrc_noidraw <- "bhrc_raw/bhrc_noidraw_v0.2_lng.rds"

# Load a summary factor formula
sumfac <- function(df, ..., .label = F, .max = 6) {
  # setting
  if (...length() ==0) dots <- tidyselect::vars_select(names(df), everything())
  else dots <- tidyselect::vars_select(names(df), ...)
  
  # formula
  if (.label == F) select(df, all_of(dots)) %>% sjlabelled::as_factor() %>% summary(maxsum = .max) 
  else select(df, all_of(dots)) %>% sjlabelled::as_label(prefix=T) %>% summary(maxsum = .max)
}

# Filter wave function
wave <- function(df, wave) {
  wave <- case_when(wave == -1 ~ "screening_arm_1",
                    wave == 0 ~ "wave0_arm_1",
                    wave == 1 ~ "wave1_arm_1",
                    wave == 2 ~ "wave2_arm_1")
  df %>% filter(redcap_event_name == wave)
}

# Create tables functions
cat("Script created by André Rafael Simioni (andresimi@gmail.com)
============================================================
Dependencies: tidyverse, huxtable, finalfit, rlang
Create table: t1 <- create_tab(data, ... = eplanatory vars, dependent = 'dependent var', ic= 1st line bottom border initial column, caption, footnotes)
")
create_tab <- function(df, ..., dependent = NULL, ic=3, caption="Table 1: Descriptive table", footnotes="", total_col=F, p=F, na_include=T) {
  library(finalfit)
  explanatory <- select_vars(names(df), !!! quos(...))
  
  footnotes <- str_c("Notes: ", footnotes)
  df %>% 
    sjlabelled::as_label(prefix = T) %>% 
    sjlabelled::set_label(label = rep(NA_character_, ncol(.))) %>% 
    summary_factorlist(explanatory = explanatory, dependent = dependent, column = T, total_col=total_col, p=p, na_include=na_include) %>% 
    create_huxtab(ic, caption, footnotes)
}

create_huxtab <- function(df, ic, caption, footnotes) {
  library(huxtable)  
  df %>% 
    as_hux(add_colnames = T) %>% 
    set_bold(row = 1, col = everywhere, value = T) %>%
    # borders
    set_top_border(val = 2, row=1, col = everywhere) %>% 
    set_bottom_border(val = 1, row = 1, col = ic:ncol(.)) %>%
    # aligns
    set_valign(val = "middle") %>% 
    set_align(val = "center", col = ic:ncol(.), row = everywhere) %>%
    # caption
    set_caption(caption) %>% 
    set_caption_pos("topleft") %>% 
    add_footnote(footnotes) %>%
    set_font_size(10)
}

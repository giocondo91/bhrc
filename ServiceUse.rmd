---
title: "Service Use"
author: "André Simioni"
date: "03/08/2020"
output: html_document
---

# Librarys and settings
First, read the README.md section about `settings.r` file.
```{r lib,  include=FALSE}
setwd("~/Documentos/pCloud/Projetos/Analises/bhrc")

# Aditional librarys

# Load settings.r
source("settings.r", echo = T)
```

# Get data from Redcap
```{r}
service_raw <- import(bhrc_noidraw, setclass = "tibble") %>% 
  filter(redcap_event_name == "wave2_arm_1") %>%
  select(ident, redcap_event_name, 
         apc_usqt1:apc_fs13)
service_raw %>% names
```

# Export
```{r}
service_raw %>% export("bhrc_data/Proc/ServiceUse.rds")
```


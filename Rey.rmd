---
title: "Rey Complex Figure"
author: "André Simioni"
date: "14 de dezembro de 2018"
output: html_document
---
# Librarys and settings
First, read the README.md section about `settings.r` file.
```{r lib,  include=FALSE}
# Aditional librarys

# Load settings.r
setwd("/home/andresimi/Documentos/pCloud/Projetos/Analises/bhrc")
source("settings.r", echo = T)
```

# Open raw data
```{r}
rey_raw <- import(bhrc_noidraw, setclass = "tibble") %>% 
  filter(redcap_event_name == "wave0_arm_1") %>% 
  select(ident, redcap_event_name, p_bqss_c:p_reyt_t_ns, -ends_with("_ns"))
rey_raw %>% sumfac()
```

# Compute
```{r}
rey <- rey_raw %>% 
  rec(starts_with("p_bqss"), starts_with("p_sava"), starts_with("p_rel"), rec = "88,99,77=NA; else=copy", suffix = "")

rey %>% sumfac
rey %>% names()

luiza <- import("Scripts/Rey/HRC-Alzheimer_26Dec.sav", setclass = "tibble") %>% 
  select(starts_with("rey"))
luiza %>% names()
luiza %>% get_labels()

```
## Copy
```{r}

```
## Imediate Recall
```{r}
rey_w0_lv <- 

rey_ir_model_adj <- 'ir =~ rey_ir1_adj + rey_ir2_adj + rey_ir3_adj + rey_ir4_adj + rey_ir5_adj + rey_ir6_adj + rey_ir7_adj + rey_ir8_adj + rey_ir9_adj + rey_ir10_adj + rey_ir11_adj + rey_ir12_adj + rey_ir13_adj + rey_ir14_adj + rey_ir15_adj + rey_ir16_adj + rey_ir17_adj + rey_ir18_adj'


```

## Late Recall
```{r}

```

# Export
```{r}
rey %>% export("bhrc_data/Proc/Rey.rds")
```


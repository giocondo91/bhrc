# Brazilian High Risk Cohort for Psychiatric Disorders (BHRC) -- codes repository
The Brazilian High Risk Cohort Study for Childhood Psychiatric Disorders (HRC) is a large community school-based study that is following 2,511 children from Brazil since 2010. We obtained psychological, genetic, and neuroimaging data with the aim to investigate typical and atypical trajectories of psychopathology and cognition over development.
We used a two-stage design. We first assessed childhood symptoms and family  history of psychiatric disorders in a screening interview, collecting information from 9,937 index children at 57 schools in the cities of São Paulo and Porto Alegre, as well as from 45,394 family members. In the  second stage, a random subsample (intended to be representative of the community, n = 958) and a high-risk subsample (children at increased risk for mental disorders, based on family risk and childhood symptoms, n = 1554) were selected for further evaluation. We evaluated those 2,512 subjects using an extensive protocol, involving one 2-h home evaluation with the parents, two 1-h evaluations of the child by a psychologist, and two 1-h evaluations of the child by a speech pathologist. In those evaluations, saliva samples for genetic studies were collected from the subjects and their biological parents. In addition, 750 children were invited to take part in a neuroimaging study and to provide blood samples for the assessment of peripheral blood biomarkers. Those children were collectively designated the enriched imaging cohort. We just completed our 3-year follow up with 80% retention. 

# URLs
- Website (https://osf.io/ktz5h/): To ask for BHRC data, or have acces to the protocols and other information;
- [BHRC Wiki](https://gitlab.com/bhrc/bhrc/-/wikis/home)

# Repositories
- BHRC Codes repository: https://gitlab.com/bhrc/bhrc  
- BHRC Data repository (restricted): https://gitlab.com/bhrc/bhrc_data  
- BHRC Raw Data repository (restricted): https://gitlab.com/bhrc/bhrc_raw  

Obs: only the code repository is open to the interested partners review the code. The *bhrc_data directory* and the *bhrc_raw directory* with the raw data is restricted by the data managers.

# Setting codes to your environment
At the beginning of every BHRC code there is a header like this, evoking the [`setting.r`](https://gitlab.com/bhrc/bhrc/-/blob/master/settings.r) file.

```{r lib,  include=FALSE}
# Aditional librarys

# Load settings.r
source("settings.r", echo = T)
```

To adapt the codes to your environment, you should copy the [`setting.r`](https://gitlab.com/bhrc/bhrc/-/blob/master/settings.r) file to the same folder where *bhrc repository* was cloned and edit the follwing lines with the path to this folder in your environment (your working directory). This way, you will need to edit only one file to adapt all codes.
> setwd("~/Documentos/pCloud/Projetos/Analises/bhrc") 
